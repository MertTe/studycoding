package com.example.miniProject.repositories;

import com.example.miniProject.models.Users;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
@Transactional
public class UsersRepository
{
    @Autowired
    UsersCRUDRepository usersCRUDRepository;

    @Autowired
    EntityManager entityManager;

    public List<Users> getUserList(){

        return (List<Users>) usersCRUDRepository.findAll();
    }
}
