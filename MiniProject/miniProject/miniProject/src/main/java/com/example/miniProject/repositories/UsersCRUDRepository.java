package com.example.miniProject.repositories;

import com.example.miniProject.models.Users;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsersCRUDRepository extends CrudRepository<Users, Long> {
}
