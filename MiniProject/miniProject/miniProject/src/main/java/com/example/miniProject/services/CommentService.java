package com.example.miniProject.services;
import com.example.miniProject.models.Comment;
import com.example.miniProject.repositories.CommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CommentService {

    @Autowired
    CommentRepository commentRepository;
    public List<Comment> getCommentByUserID(@PathVariable Long userID)//
    {
        return commentRepository.getAllComments().stream()
                .filter(comment -> comment.getUsers().getUserID()
                        .equals(userID)).collect(Collectors.toList());
    }
}
