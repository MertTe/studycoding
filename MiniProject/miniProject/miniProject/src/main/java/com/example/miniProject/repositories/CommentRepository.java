package com.example.miniProject.repositories;

import com.example.miniProject.models.Comment;
import com.example.miniProject.models.Users;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Transactional
@Repository
public class CommentRepository {

    @Autowired
    EntityManager entityManager;
    @Autowired
    CommentCRUDRepository commentCRUDRepository;

    public List<Comment> getAllComments(){
        return (List<Comment>) commentCRUDRepository.findAll();
    }

    public void newComment(Long commentID,Long userID) throws NullPointerException{
        try {
            Users users = entityManager.find(Users.class,userID);
            Comment comment = new Comment(commentID,users);
            commentCRUDRepository.save(comment);
        }catch (Exception e){
            System.err.println("Fehler: "+e.getCause());
        }
    }

    public List<Comment> getCommentNickname(){
        List<Comment> c = (List<Comment>) commentCRUDRepository.findAll();
        return  c.stream().filter(comment -> comment.getUsers().getUserID().equals(comment.getUsers().getUserID())).collect(Collectors.toList());
    }

}
