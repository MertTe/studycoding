package com.example.miniProject.controller;

import com.example.miniProject.models.Comment;
import com.example.miniProject.models.Users;
import com.example.miniProject.repositories.CommentCRUDRepository;
import com.example.miniProject.repositories.CommentRepository;
import com.example.miniProject.repositories.UsersCRUDRepository;
import com.example.miniProject.repositories.UsersRepository;
import com.example.miniProject.services.CommentService;
import com.example.miniProject.services.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.stream.Collectors;


@RequestMapping("/api/Users")
@RestController
@CrossOrigin
public class Controller {

    @Autowired
    UsersCRUDRepository usersCRUDRepository;
    @Autowired
    UsersRepository usersRepository;
    @Autowired
    CommentRepository commentRepository;
    @Autowired
    CommentCRUDRepository commentCRUDRepository;

    private final UsersService usersService;
    private final CommentService commentService;

    public Controller(UsersService usersService, CommentService commentService) {
        this.usersService = usersService;
        this.commentService = commentService;
    }

    //gets all user
    @CrossOrigin
    @GetMapping(path = "/users/")
    public List<Users> getUsers()
    {
     return usersService.getUsers();
    }

    //returns user > 25
    @CrossOrigin
    @GetMapping(path = "/overTwentyFive/")
    public List<Users> getUserOverTwentyFive(){
        return usersService.getUserOverTwentyFive();
    }

    //returns user < 25
    @CrossOrigin
    @GetMapping(path = "/underTwentyFive/")
    public List<Users> getUnderTwentyFive(){
        return usersService.getUnderTwentyFive();
    }

    //not sure which throw is ok
    //new user is not blocked
    //did only nickname validation, other values are not nullable.
    @CrossOrigin
    @PostMapping(path = "/newUser/")
        public String inputUser(@RequestBody Users users) {
        return usersService.inputUser(users);
    }

    //finds user by name that is not blocked
    @CrossOrigin
    @PostMapping(path = "/findUser/{name}")
    public List<Users> findUserByName(@PathVariable String name){
        return usersService.findUserByName(name);
    }

    //finds user that are blocked
    @CrossOrigin
    @GetMapping(path = "/blockedUser/")
    public List<Users> showBlockedUser(){
        return usersService.showBlockedUser();
    }

    //SERVICE AND CONTROLLER NEEDED
    //Getting comments written by user (with userID) personal -> other can't see this only the main account user
    @CrossOrigin
    @GetMapping(path = "/commentsByUser/{userID}")//
    public List<Comment> getCommentByUserID(@PathVariable Long userID)
    {
     return commentService.getCommentByUserID(userID);
    }

   /* @CrossOrigin
    @GetMapping(path = "/banUser/{id}")
    public String blockUser(@PathVariable Long id){
       Users a = (Users) usersRepository.getUserList().stream().filter(users -> users.getUserID().equals(id)).collect(Collectors.toList());
       a.setBlocked(true);
       return a.getNickname()+" Blocked";
    }*/

}
