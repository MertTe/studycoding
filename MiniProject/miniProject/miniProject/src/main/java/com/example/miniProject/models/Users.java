package com.example.miniProject.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;

import java.util.Set;

//going to work low level on this project so basic information needed
@Entity
@Table
public class Users
{
    //id
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long userID;

    //name
    @Column(nullable = false)
    private String name;

    //nickname
    @Column(unique = true, length = 12, nullable = false)
    private String nickname;

    //age
    @Column(nullable = false)
    private Integer age;

    //address
    @Column(nullable = false)
    private String address;

    //blocked
    @Column(nullable = true)
    private Boolean blocked;

    //relation with comment
    @JsonManagedReference
    @OneToMany(mappedBy = "users",cascade = CascadeType.ALL)
    private Set<Comment> comments;

    //constructor to create
    public Users(String name, String nickname ,Integer age, String address, Boolean blocked) {
        this.name = name;
        this.nickname=nickname;
        this.age = age;
        this.address = address;
        this.blocked = blocked;
    }

    //constructor to work
    public Users() {
    }

    public Long getUserID() {
        return userID;
    }

    public void setUserID(Long id) {
        this.userID = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Boolean getBlocked() {
        return blocked;
    }

    //comment relation


    public Set<Comment> getComments() {
        return comments;
    }

    public void setComments(Comment comments) {
        this.comments.add(comments);
    }

    public void setBlocked(Boolean blocked) {
        this.blocked = blocked;
    }

    //for being sure it is saved
    @Override
    public String toString() {
        return "Create User: " +
                ", name='" + name + '\'' +
                ", nickname='" +nickname+
                ", age=" + age +
                ", address='" + address;
    }
}
