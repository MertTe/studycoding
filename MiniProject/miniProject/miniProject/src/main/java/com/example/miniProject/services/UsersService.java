package com.example.miniProject.services;
import com.example.miniProject.models.Users;
import com.example.miniProject.repositories.UsersCRUDRepository;
import com.example.miniProject.repositories.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UsersService {

    @Autowired
    UsersCRUDRepository usersCRUDRepository;
    @Autowired
    UsersRepository usersRepository;


    public List<Users> getUsers()
    {
        return (List<Users>) usersCRUDRepository.findAll();
    }

    public List<Users> getUserOverTwentyFive(){
        return usersRepository.getUserList().stream().filter(u -> u.getAge()>25).collect(Collectors.toList());
    }

    public List<Users> getUnderTwentyFive(){
        return usersRepository.getUserList().stream().filter(u -> u.getAge()<25).collect(Collectors.toList());
    }

    //adds User by checking up nickname and length
    public String inputUser(@RequestBody Users users) {
        try {
            if (users.getNickname().length()>0 && users.getNickname().length()<=12 && !users.getNickname().equals(""))
            {
                users.setBlocked(Boolean.FALSE);
                usersCRUDRepository.save(new Users(users.getName(),users.getNickname(),users.getAge(), users.getAddress(), users.getBlocked()));
                return "Successful > User Added: "+users.getNickname();
            }
        }catch (Exception e){
            System.err.println("Fail: "+e.getCause());
            return "Failure > Necessary information might be missing or nickname could be already taken";
        }
        return "Data or Nickname might be a problem check first typed data or nickname length. - min 1 max 12 characters: "
                +users.getNickname()+" length: "+users.getNickname().length();
    }

    public List<Users> findUserByName(@PathVariable String name){
        return usersRepository.getUserList().stream().filter(u-> u.getName().equals(name))
                .filter(u-> u.getBlocked().booleanValue()==Boolean.FALSE)
                .collect(Collectors.toList());
    }

    public List<Users> showBlockedUser(){
        return usersRepository.getUserList().stream()
                .filter(Users::getBlocked)
                .collect(Collectors.toList());
    }
}
