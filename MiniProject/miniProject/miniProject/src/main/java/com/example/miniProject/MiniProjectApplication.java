package com.example.miniProject;

import com.example.miniProject.models.Comment;
import com.example.miniProject.models.Users;
import com.example.miniProject.repositories.CommentCRUDRepository;
import com.example.miniProject.repositories.CommentRepository;
import com.example.miniProject.repositories.UsersCRUDRepository;
import jakarta.persistence.EntityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MiniProjectApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(MiniProjectApplication.class, args);
	}

	@Autowired
	UsersCRUDRepository usersCRUDRepository;
	@Autowired
	CommentCRUDRepository commentCRUDRepository;
	@Autowired
	CommentRepository commentRepository;


	@Override
	public void run(String... args) throws Exception {
		//String name, Integer age, String address
		usersCRUDRepository.save(new Users("John","JJ",25,"Addaddress123",Boolean.FALSE));
		usersCRUDRepository.save(new Users("Marta","TheRealMJ",23,"OneStreet123",Boolean.FALSE));
		usersCRUDRepository.save(new Users("Carl","CarlJohnson",27,"Streets12",Boolean.FALSE));
		usersCRUDRepository.save(new Users("Jackson","JJ12",27,"Streets12",Boolean.FALSE));
		usersCRUDRepository.save(new Users("Jackson","JJ13",20,"Streets13",Boolean.FALSE));
		usersCRUDRepository.save(new Users("Jackson","BadJackson",19,"BadStreet12",Boolean.TRUE));

		//relation created with comment


		commentRepository.newComment(1L,1L);
		commentRepository.newComment(2L,1L);
		commentRepository.newComment(2L,1L);
		commentRepository.newComment(2L,2L);

	}
}
